cube(`Country`, {
  sql: `SELECT * FROM sakila.country`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [country, id, lastUpdate]
    }
  },
  
  dimensions: {
    country: {
      sql: `country`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    lastUpdate: {
      sql: `last_update`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
