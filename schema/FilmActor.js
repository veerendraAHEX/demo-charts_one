cube(`FilmActor`, {
  sql: `SELECT * FROM sakila.film_actor`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    Film: {
      sql: `${CUBE}.film_id = ${Film}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, lastUpdate]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    lastUpdate: {
      sql: `last_update`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
