cube(`City`, {
  sql: `SELECT * FROM sakila.city`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    Country: {
      sql: `${CUBE}.country_id = ${Country}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [city, id, lastUpdate]
    }
  },
  
  dimensions: {
    city: {
      sql: `city`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    lastUpdate: {
      sql: `last_update`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
