cube(`Film`, {
  sql: `SELECT * FROM sakila.film`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    Language: {
      sql: `${CUBE}.language_id = ${Language}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, title, lastUpdate]
    },
    
    rentalDuration: {
      sql: `rental_duration`,
      type: `sum`
    },
    
    replacementCost: {
      sql: `replacement_cost`,
      type: `sum`
    }
  },
  
  dimensions: {
    description: {
      sql: `description`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    rating: {
      sql: `rating`,
      type: `string`
    },
    
    releaseYear: {
      sql: `release_year`,
      type: `string`
    },
    
    specialFeatures: {
      sql: `special_features`,
      type: `string`
    },
    
    title: {
      sql: `title`,
      type: `string`
    },
    
    lastUpdate: {
      sql: `last_update`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
