cube(`Rental`, {
  sql: `SELECT * FROM sakila.rental`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    Customer: {
      sql: `${CUBE}.customer_id = ${Customer}.id`,
      relationship: `belongsTo`
    },
    
    Inventory: {
      sql: `${CUBE}.inventory_id = ${Inventory}.id`,
      relationship: `belongsTo`
    },
    
    Staff: {
      sql: `${CUBE}.staff_id = ${Staff}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, lastUpdate, rentalDate, returnDate]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    lastUpdate: {
      sql: `last_update`,
      type: `time`
    },
    
    rentalDate: {
      sql: `rental_date`,
      type: `time`
    },
    
    returnDate: {
      sql: `return_date`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
