cube(`Inventory`, {
  sql: `SELECT * FROM sakila.inventory`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    Film: {
      sql: `${CUBE}.film_id = ${Film}.id`,
      relationship: `belongsTo`
    },
    
    Store: {
      sql: `${CUBE}.store_id = ${Store}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, lastUpdate]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    lastUpdate: {
      sql: `last_update`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
