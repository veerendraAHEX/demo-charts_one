cube(`Payment`, {
  sql: `SELECT * FROM sakila.payment`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    Customer: {
      sql: `${CUBE}.customer_id = ${Customer}.id`,
      relationship: `belongsTo`
    },
    
    Rental: {
      sql: `${CUBE}.rental_id = ${Rental}.id`,
      relationship: `belongsTo`
    },
    
    Staff: {
      sql: `${CUBE}.staff_id = ${Staff}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, lastUpdate, paymentDate]
    },
    
    amount: {
      sql: `amount`,
      type: `sum`
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    lastUpdate: {
      sql: `last_update`,
      type: `time`
    },
    
    paymentDate: {
      sql: `payment_date`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
